const { app } = require('./app');

const log = require('debug')('log');

if (process.env.SERVER_APP_PORT) {
  app.listen(process.env.SERVER_APP_PORT, '0.0.0.0');
  log(` App listening on port ${process.env.SERVER_APP_PORT}`);
} else {
  const port = process.env.PORT || 8003;
  app.listen(port);
  log(` App listening on port ${port}`);
}
