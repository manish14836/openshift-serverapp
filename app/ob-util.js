process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0; // To enable use of self signed certs

const APPLICATION_JSON = 'application/json';

const mtlsEnabled = process.env.MTLS_ENABLED === 'true';
const ca = Buffer.from(process.env.OB_ISSUING_CA || '', 'base64').toString();
const cert = Buffer.from(process.env.TRANSPORT_CERT || '', 'base64').toString();
const key = () => Buffer.from(process.env.TRANSPORT_KEY || '', 'base64').toString();

const setupMutualTLS = agent => (mtlsEnabled ? agent.key(key()).cert(cert).ca(cert) : agent);

const setOptionalHeader = (header, value, requestObj) => {
  if (value) requestObj.set(header, value);
};

const setHeaders = (requestObj, headers) => {

  let ipaddress = headers.customerIpaddress;

  requestObj
   // .set('authorization', `Bearer ${headers.accessToken}`)
    .set('content-type', APPLICATION_JSON)
    .set('accept', APPLICATION_JSON)
  return requestObj;
};

const createRequest = (requestObj, headers) => {
  const req = setHeaders(setupMutualTLS(requestObj), headers);
  return req;
};


const obtainResult = async (call, response, headers) => {
  let result;
  result = response.body;
  return result;
};

exports.setupMutualTLS = setupMutualTLS;
exports.createRequest = createRequest;
exports.obtainResult = obtainResult;
exports.caCert = ca;
exports.clientCert = cert;
exports.clientKey = key;
exports.mtlsEnabled = mtlsEnabled;
