if (!process.env.DEBUG) process.env.DEBUG = 'error,log';

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { requireAuthorization } = require('./session');
const { login } = require('./session');
const { RequestHandler,fundsconfirmationRequestHandler} = require('./request-data/ob-proxy.js');

const app = express();

app.options('*', cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/login', login.authenticate);
app.get('/logout', login.logout);

app.all('/api/*', requireAuthorization);
app.get('/api/*', RequestHandler);



exports.app = app;
