const uuidv4 = require('uuid/v4');
const { getUsername } = require('./session');

const extractHeaders = async (headers) => {
  const sessionId = headers['authorization'];
  const username = await getUsername(sessionId);
  return {
    sessionId, username
  };
};

exports.extractHeaders = extractHeaders;
