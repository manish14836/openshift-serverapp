/*
POST a username and password (form encoded)
if the username and password match - create a session cookie
 */

const { session,extractHeaders} = require('./session');
const log = require('debug')('log');
const { createRequest, obtainResult } = require('../ob-util');
const superagent = require('superagent');
const uri =(process.env.AUTHENTICATION_API || "http://l20118:8305");
const proxiedUrl = `${uri}/authentication/v1/login`;

const login = (() => {
  const  authenticate = async (req, res) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      let response;
    try {
    const reqHeaders = {};
    const { u } = req.body;
    const { p } = req.body;
    let apirequest = createRequest(superagent.get(proxiedUrl), reqHeaders);
         apirequest.set('username', u);
         apirequest.set('password', p);
         response = await apirequest.send();
    if (response.status === 200) {
        let sid = session.newSession(u);
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(JSON.stringify({ "sid":sid,"customer" : response.text}));
    } else if (u === 'trigger-error') {
      res.status(500).send();
    }
  } catch (err) {
     const status = err.response ? err.response.status : 500;
     return res.status(status).send({ responseText: err.response.text });
  }
  res.status(401).send();
};

  const logout = (req, res) => {

    res.setHeader('Access-Control-Allow-Origin', '*');
    const sid = req.headers['authorization'];
        //console.log(`in logout sid is ${sid}`);
    session.destroy(sid, (sidConf) => {
      if (sidConf) {

        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(JSON.stringify({ sid: sidConf }));
      } else {
        res.sendStatus(204);
      }
    });
  };

  return {
    authenticate,
    logout,
  };
})();

exports.login = login;
