const loki = require("lokijs");
const log = require('debug')('log');



var db = new loki('usersessions.db', {
	autoload: true,
	autoloadCallback : databaseInitialize,
	autosave: true,
	autosaveInterval: 4000
});

let users = null;
// implement the autoloadback referenced in loki constructor
function databaseInitialize() {
  db.removeCollection("sessions");
    users = db.getCollection("sessions");
  if (users === null) {
    users = db.addCollection("sessions");
  }
}

const noop = () => {
  console.log("Removal done");

};

const store = (() => {
  const set = (key, value, cb) => {
    const cbk = cb || noop;
    if (typeof key !== 'string') throw new Error(' key must be of type String ');
    if (typeof value !== 'string') throw new Error(' value must be of type String ');
    users.insert({'key':key ,'value':value});
    return cbk();
  };

  const get = (keyvalue, cb) => {
    const cbk = cb || noop;
    if (!keyvalue) return cbk(null, null);
    console.log(keyvalue);
    var result = users.find({ key : keyvalue});
    if(typeof(result[0]) === "undefined") {
      return cbk(null, null);
    }
     return cbk(null,result[0].value);
  };

  const remove = key =>
  {
   users.chain().find({ 'key': key }).remove();
    console.log("sdzs");
    log(`Exit`);
}
  const getAll = (cb) => {
    const cbk = cb || noop;
    client.keys('*', cbk);
  };

  const deleteAll = async () =>
    new Promise(resolve => client.flushall(resolve));

  return {
    set,
    get,
    remove
  };
})();

exports.store = store;
