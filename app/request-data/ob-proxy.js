const request = require('superagent');
const { createRequest, obtainResult } = require('../ob-util');
const { extractHeaders } = require('../session');
const debug = require('debug')('debug');
const error = require('debug')('error');
const uri =(process.env.ACCOUNT_API || "http://l20118:8305");
const RequestHandler = async (req, res) => {
  try {
    res.setHeader('Access-Control-Allow-Origin', '*');
    const reqHeaders = await extractHeaders(req.headers);

    const path =  `${req.path}`;
    const proxiedUrl = `${uri}/${path}`;
    console.log(proxiedUrl);
    const {interactionId,username } = reqHeaders;
    let apiRequestResponse = await callApi(proxiedUrl,reqHeaders);
     const result = await obtainResult(apiRequestResponse.apirequest, apiRequestResponse.response, reqHeaders);
    return res.status(apiRequestResponse.response.status).json({"response":result});

  } catch (err) {
    const status = err.response ? err.response.status : 500;
    return res.status(status).send({ responseText: err.response.text });
  }
};

const callApi = async (proxiedUrl,headers,query) => {

  const apirequest = createRequest(request.get(proxiedUrl), headers);
  if(query !==null) {
    apirequest.query(query);
  }

  let response;
  try {
    response = await apirequest.send();
  } catch (err) {
      console.log(`error getting ${proxiedUrl}: ${err.message}`,err.response.status);
  }
    return {response, apirequest};

};

exports.callApi =callApi;
exports.RequestHandler = RequestHandler;
